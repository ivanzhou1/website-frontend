import Head from "next/head";
import Link from "next/link";
import { Trans } from "react-i18next";
import Explanation from "../lib/components/Explanation";
import H1 from "../lib/components/H1";
import VerticallyCentered from "../lib/components/VerticallyCentered";
import page from "../lib/hocs/page";

export default page(["_error", "common"])(({ t }) => (
  <VerticallyCentered>
    <Head>
      <title>{t("title")}</title>
    </Head>
    <H1 error={true}>{t("h1")}</H1>
    <Explanation>
      <Trans i18nKey="explanation">
        <a href="mailto:alexander@kachkaev.ru">email</a>
      </Trans>
      <br />
      <br />
      <Link href="/">
        <a>{t("common:signature")}</a>
      </Link>
    </Explanation>
  </VerticallyCentered>
));

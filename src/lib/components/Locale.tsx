import { translate } from "react-i18next";
import styled from "styled-components";
import { format } from "url";

const CurrentLocale = styled.span`
  margin-left: 8px;
`;
const LinkToLocale = styled.a`
  margin-left: 8px;
`;

const Locale = ({ targetLocale, i18n, host, currentUrl }) =>
  targetLocale === i18n.language ? (
    <CurrentLocale key={targetLocale}>{targetLocale}</CurrentLocale>
  ) : (
    <LinkToLocale
      key={targetLocale}
      href={format({ protocol: "https:", ...currentUrl, hostname: host })}
    >
      {targetLocale}
    </LinkToLocale>
  );

export default translate()(Locale);

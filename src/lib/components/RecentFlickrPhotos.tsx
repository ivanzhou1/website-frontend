import get from "lodash/get";
import map from "lodash/map";
import styled from "styled-components";

const Wrapper = styled.div`
  line-height: 1em;
  margin-top: 5px;
  border-radius: 5px;
  background-clip: padding-box;
  overflow: hidden;
  height: 50px;
`;

const PhotoA = styled.a`
  display: inline-block;
  width: 50px;
  height: 50px;
  vertical-align: bottom;
  border: none !important;
  position: relative;
`;
const PhotoImg = styled.img`
  filter: grayscale(100%);
  a:hover &,
  a:active & {
    filter: none;
  }
`;
const PhotoUnderline = styled.span`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  display: block;

  a:hover & {
    border-top: 2px solid red;
  }
`;

const RecentFlickrPhotos = ({ info }) => (
  <Wrapper>
    {map(get(info, "recentPhotos"), ({ title, url, thumbnailUrl }, index) => (
      <PhotoA href={url} title={title} key={index}>
        <PhotoImg width="50" height="50" alt={title} src={thumbnailUrl} />
        <PhotoUnderline />
      </PhotoA>
    ))}
  </Wrapper>
);

export default RecentFlickrPhotos;

import Head from "next/head";
import { Fragment } from "react";
import styled from "styled-components";
import Description from "../lib/components/Description";
import H1 from "../lib/components/H1";
import Photo from "../lib/components/Photo";
import Profiles from "../lib/components/Profiles";
import VerticallyCentered from "../lib/components/VerticallyCentered";
import page from "../lib/hocs/page";

const LastName = styled.span`
  display: inline-block;
  position: relative;
`;

const Wrapper = styled.div`
  width: 450px;
  margin: 0 auto 10px;
  padding-top: 10px;
`;

const LastNamePronunciation = styled.div`
  position: absolute;
  font-size: 13px;
  bottom: -20px;
  left: 0;
  right: 0;
  text-align: center;
  opacity: 0.5;
  font-weight: normal;
`;
const LastNamePronunciationBracket = styled.span`
  display: none;
`;

export default page(["index", "common"])(({ t, i18n }) => (
  <VerticallyCentered>
    <Wrapper>
      <Head>
        <title>{t("title")}</title>
        <meta name="description" content={t("description")} />
      </Head>
      <H1>
        {t("h1.firstName")}{" "}
        <LastName>
          {t("h1.lastName")}
          {i18n.language === "en" ? (
            <Fragment>
              {" "}
              <LastNamePronunciation key="last-name">
                <LastNamePronunciationBracket>(</LastNamePronunciationBracket>
                catch · ka ′ yev
                <LastNamePronunciationBracket>)</LastNamePronunciationBracket>
              </LastNamePronunciation>
            </Fragment>
          ) : null}
        </LastName>
      </H1>
      <Description />
      <Photo />
      <Profiles />
    </Wrapper>
  </VerticallyCentered>
));

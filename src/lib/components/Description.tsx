import { Trans } from "react-i18next";
import styled from "styled-components";

const Wrapper = styled.div`
  padding-top: 12px;
  padding-bottom: 28px;
`;

export default () => (
  <Wrapper>
    <Trans i18nKey="description.l1" />
    <br />
    <Trans i18nKey="description.l2">
      <a href="http://www.gicentre.net/">giCentre</a>
    </Trans>
    <br />
    <Trans i18nKey="description.l3" />
  </Wrapper>
);

import { Fragment } from "react";
import { Trans } from "react-i18next";
import styled from "styled-components";

const Bold = styled.span`
  font-weight: bold;
`;

export default ({ profileName }: { profileName: string }) => (
  <Fragment>
    <Bold>
      <Trans i18nKey={`profiles.${profileName}.name`} />
    </Bold>:{" "}
  </Fragment>
);

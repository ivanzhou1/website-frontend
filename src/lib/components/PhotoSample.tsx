import styled from "styled-components";

const Frame = styled.a`
  display: block;
  width: 400px;
  height: 250px;
  line-height: 250px;
  display: inline-block;
  background: #eee;
  border: none !important;
  position: relative;
  border-radius: 5px;
  overflow: hidden;
  margin-bottom: 1.5em;
`;

const Label = styled.span`
  color: rgba(255, 255, 255, 0.8) !important;
  position: absolute;
  right: 1.7em;
  bottom: 0.3em;
  line-height: 1.5em;
  transform: rotate(-90deg);
  transform-origin: bottom left;
  height: 0;
  width: 0;
  white-space: nowrap;
  overflow: visible;
  text-shadow: 1px 1px 5px black;
`;

const Image = styled.img`
  display: inline-block;
  vertical-align: middle;
  width: 400px;
  filter: grayscale(100%);
  a:hover &,
  a:active & {
    filter: none;
  }
`;

const Underline = styled.span`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  display: block;

  a:hover & {
    border-top: 2px solid red;
  }
`;

export default () => (
  <Frame href="https://www.flickr.com/photos/kachkaev/7511763574/">
    <Image src="https://farm8.staticflickr.com/7247/7511763574_d528f4ce04_z_d.jpg" />
    <Label>kachkaev.ru/photos</Label>
    <Underline />
  </Frame>
);

/* color: ${(p) => (p.error ? 'grey' : 'black')}; */

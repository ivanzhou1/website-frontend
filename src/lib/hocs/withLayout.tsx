import { TranslationFunction } from "i18next";
import React from "react";
import styled from "styled-components";
import LinkToSources from "../components/LinkToSource";
import LocaleToggler from "../components/LocaleToggler";

const Wrapper = styled.div`
  display: table;
  width: 100%;
  height: 100%;
  position: relative;
`;

export default (
  ChildComponent: React.ComponentClass<{ t: TranslationFunction }>,
) => {
  const Component = (props) => (
    <Wrapper>
      <LocaleToggler {...props} />
      <ChildComponent {...props} />
      <LinkToSources />
    </Wrapper>
  );
  (Component as any).getInitialProps = (ChildComponent as any).getInitialProps;
  return Component;
};

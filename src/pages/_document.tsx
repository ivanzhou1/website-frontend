import { readFileSync } from "fs";
import Document, { Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";
import removeCommentsAndSpacing from "../lib/removeCommentsAndSpacing";

// prevent caching in production
// https://github.com/zeit/next-plugins/issues/11#issuecomment-376125174
let style = null;
if (process.env.NODE_ENV === "production") {
  // ${"css"} prevents editors from incorrectly highlighting code after css`
  style = readFileSync(`${process.cwd()}/.next/static/style.${"css"}`, "utf8");
}

export default class MyDocument extends Document {
  public static getInitialProps({
    renderPage,
    req: { locale, localeDataScript, gaTrackingId, hostsByLocale, graphqlUri },
  }) {
    const sheet = new ServerStyleSheet();
    const { html, head, errorHtml, chunks } = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );
    const styleTags = sheet.getStyleElement();

    return {
      html,
      head,
      errorHtml,
      chunks,
      styleTags,
      locale,
      hostsByLocale,
      localeDataScript,
      gaTrackingId,
      graphqlUri,
    };
  }

  public render() {
    return (
      <html lang={this.props.locale}>
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=500" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="black-translucent"
          />
          <link
            rel="apple-touch-icon"
            sizes="57x57"
            href="/static/favicon/apple-icon-57x57.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="60x60"
            href="/static/favicon/apple-icon-60x60.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="72x72"
            href="/static/favicon/apple-icon-72x72.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/static/favicon/apple-icon-76x76.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="114x114"
            href="/static/favicon/apple-icon-114x114.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="120x120"
            href="/static/favicon/apple-icon-120x120.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="144x144"
            href="/static/favicon/apple-icon-144x144.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="152x152"
            href="/static/favicon/apple-icon-152x152.png"
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/static/favicon/apple-icon-180x180.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href="/static/favicon/android-icon-192x192.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/static/favicon/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="96x96"
            href="/static/favicon/favicon-96x96.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/static/favicon/favicon-16x16.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta
            name="msapplication-TileImage"
            content="/static/favicon/ms-icon-144x144.png"
          />
          <meta name="theme-color" content="#ffffff" />
          {typeof style === "string" ? (
            <style dangerouslySetInnerHTML={{ __html: style }} />
          ) : (
            <link rel="stylesheet" href="/_next/static/style.css" />
          )}
          {this.props.styleTags}
          {this.props.gaTrackingId ? (
            [
              <script
                key="ga1"
                async={true}
                src={`https://www.googletagmanager.com/gtag/js?id=${
                  this.props.gaTrackingId
                }`}
              />,
              <script
                key="ga2"
                dangerouslySetInnerHTML={{
                  __html: removeCommentsAndSpacing(`
window.dataLayer = window.dataLayer || [];
window.gaTrackingId = '${this.props.gaTrackingId}';
function gtag(){
  dataLayer.push(arguments)
}
gtag('js', new Date());
gtag('config', window.gaTrackingId);`),
                }}
              />,
            ]
          ) : (
            <script
              dangerouslySetInnerHTML={{
                __html: removeCommentsAndSpacing(`
function gtag(){
  console.log('dummy gtag call', arguments)
}`),
              }}
            />
          )}
        </Head>
        <body>
          <div className="next-main">
            <Main />
          </div>
          <script
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: this.props.localeDataScript,
            }}
          />
          <NextScript />
        </body>
      </html>
    );
  }
}
